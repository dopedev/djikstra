//
//  AHColorsClientTest.m
//  Djikstra
//
//  Created by Anthony Hoang on 5/3/16.
//  Copyright © 2016 Anthony Hoang. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "AHColorsClient+Stub.h"


@interface AHColorsClientTest : XCTestCase

@property (nonatomic, strong) AHColorsClient *client;

@end

@implementation AHColorsClientTest

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
    
    self.client = [AHColorsClient new];
    [self.client setStubbingEnabled:YES];
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testRandomColor {
    
    XCTestExpectation *expectation = [self expectationWithDescription:[NSString stringWithFormat:@"%s expectation", __PRETTY_FUNCTION__]];
    
    [[self.client getRandomColorSignal] subscribeNext:^(AHColor* x) {
        XCTAssertNotNil(x);
        XCTAssertTrue([x.hexStringColor isEqualToString:@"ad7e42"]);
    } error:^(NSError *error) {
        XCTFail();
    } completed:^{
        [expectation fulfill];
    }];
    
    [self waitForExpectationsWithTimeout:1.0f handler:nil];
    
}

@end
