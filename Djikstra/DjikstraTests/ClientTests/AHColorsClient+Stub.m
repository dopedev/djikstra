//
//  AHColorsClient+Stub.m
//  Djikstra
//
//  Created by Anthony Hoang on 5/3/16.
//  Copyright © 2016 Anthony Hoang. All rights reserved.
//

#import "AHColorsClient+Stub.h"
#import <OHHTTPStubs/OHPathHelpers.h>

@implementation AHColorsClient (Stub)

- (void)setStubbingEnabled:(BOOL)enabled {
    
    [OHHTTPStubs setEnabled:enabled];
    
    [OHHTTPStubs stubRequestsPassingTest:^BOOL(NSURLRequest * _Nonnull request) {
        return [request.URL.absoluteString containsString:@"color/random"];
    } withStubResponse:^OHHTTPStubsResponse * _Nonnull(NSURLRequest * _Nonnull request) {
        NSString* fixture = OHPathForFile(@"color.json", self.class);
        return [OHHTTPStubsResponse responseWithFileAtPath:fixture
                                                statusCode:200 headers:@{@"Content-Type":@"text/plain"}];
    }];
    
}

@end
