//
//  AHColorsClient+Stub.h
//  Djikstra
//
//  Created by Anthony Hoang on 5/3/16.
//  Copyright © 2016 Anthony Hoang. All rights reserved.
//

#import "AHColorsClient.h"
#import <OHHTTPStubs/OHHTTPStubs.h>
/**
 *  Stubbing for AHColorsClient
 */
@interface AHColorsClient (Stub)
/**
 *  Enable stubbing.
 *
 *  @param enabled YES to enable stubbing. NO to disable stubbing
 */
- (void)setStubbingEnabled:(BOOL)enabled;

@end
