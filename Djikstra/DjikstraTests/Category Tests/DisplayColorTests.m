//
//  DisplayColorTests.m
//  Djikstra
//
//  Created by Anthony Hoang on 5/3/16.
//  Copyright © 2016 Anthony Hoang. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "UIColor+DisplayColor.h"

@interface DisplayColorTests : XCTestCase

@end

@implementation DisplayColorTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

/**
 *  If the color is black then the readable color should be white.
 */
- (void)testBlack {
    UIColor *blackColor = [UIColor blackColor];
    
    NSString *displayString = [blackColor calculateDisplayColor];
    
    XCTAssertTrue([displayString containsString:@"ffffff"]);
}
/**
 *  If the background color is white than the readable color should be black.
 */
- (void)testWhite {
    UIColor *whiteColor = [UIColor whiteColor];
    
    NSString *displayString = [whiteColor calculateDisplayColor];
    
    XCTAssertTrue([displayString containsString:@"000000"]);
}

@end
