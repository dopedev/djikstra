//
//  AHGraphViewModelTests.m
//  Djikstra
//
//  Created by Anthony Hoang on 5/3/16.
//  Copyright © 2016 Anthony Hoang. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "AHGraphViewModelProtocol.h"

/**
 *  These are tests for AHGraphViewModelProtocol. All implementations of AHGraphViewModelProtocol should subclass this test class and all tests should pass the implementation.
 */
@interface AHGraphViewModelProtocolTests : XCTestCase

@property (nonatomic, strong) id<AHGraphViewModelProtocol> graphViewModel;

- (void)testPath;
- (void)testCreateNodeSignal;

@end

@implementation AHGraphViewModelProtocolTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testExample {
    // This is an example of a functional test case.
    // Use XCTAssert and related functions to verify your tests produce the correct results.
}

- (void)testCreateNodeSignal {
    // should return a node view model.
    // should complete after
    if (self.graphViewModel != nil) {
        
        XCTestExpectation *expectation = [self expectationWithDescription:[NSString stringWithFormat:@"%s expectation", __PRETTY_FUNCTION__]];
        
        [[self.graphViewModel createNodeSignal] subscribeNext:^(id x) {
            
            XCTAssertTrue([x conformsToProtocol:@protocol(AHNodeViewModelProtocol)]);
            
        } completed:^{
            [expectation fulfill];
        }];;
        
        
        [self waitForExpectationsWithTimeout:1 handler:nil];
    }
}

- (void)testPath {
    // create nodes
    // create edges
    // get path and expect
    
    // create nodes
    for (int i = 0; i < 5; i++) {
        [[self.graphViewModel createNodeSignal] subscribeCompleted:^{
            
        }];
    }
    
    //create edges
    [self.graphViewModel createEdgeWithWeight:5 startingNodeIndex:0 endingNodeIndex:1];
    [self.graphViewModel createEdgeWithWeight:9 startingNodeIndex:1 endingNodeIndex:3];
    [self.graphViewModel createEdgeWithWeight:6 startingNodeIndex:3 endingNodeIndex:4];
    [self.graphViewModel createEdgeWithWeight:8 startingNodeIndex:4 endingNodeIndex:2];
    [self.graphViewModel createEdgeWithWeight:7 startingNodeIndex:0 endingNodeIndex:2];
    [self.graphViewModel createEdgeWithWeight:17 startingNodeIndex:0 endingNodeIndex:3];
    
    // shortest path from 0 -> 3 should be 0-1-3
    [self.graphViewModel getPathFromStartingNode:0 toEndNode:3 pathCallback:^(NSArray<NSNumber *> *nodesInPath) {
        
        XCTAssertEqual(nodesInPath[0].integerValue, 0);
        XCTAssertEqual(nodesInPath[1].integerValue, 1);
        XCTAssertEqual(nodesInPath[2].integerValue, 3);
        NSLog(@"%s,", __PRETTY_FUNCTION__);
    }];
    
    
}

@end

#import "AHGraphViewModel.h"
@interface AHGraphViewModelTests : AHGraphViewModelProtocolTests

@end

@implementation AHGraphViewModelTests

- (void)setUp {
    [super setUp];
    self.graphViewModel = [AHGraphViewModel new];
}

- (void)testPath {
    [super testPath];
}

- (void)testCreateNodeSignal {
    [super testCreateNodeSignal];
}


@end
