//
//  AHNodeView.m
//  Djikstra
//
//  Created by Anthony Hoang on 5/2/16.
//  Copyright © 2016 Anthony Hoang. All rights reserved.
//

#import "AHNodeView.h"
#import "UIColor+HexString.h"
#import <ReactiveCocoa/ReactiveCocoa.h>

@implementation AHNodeView

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    
    if (self) {
        @weakify(self)
        
        [RACObserve(self, viewModel.nodeName)
         subscribeNext:^(NSString *x) {
            @strongify(self)
             [self setTitle:x forState:UIControlStateNormal];
        }];
        
        RAC(self, backgroundColor) = [[RACObserve(self, viewModel.nodeColorHexString) ignore:nil] map:^UIColor *(NSString *value) {
            return [UIColor colorWithHexString:value];
        }];
        
        [[[RACObserve(self, viewModel.textColorHexString) ignore:nil] map:^UIColor *(NSString *value) {
            return [UIColor colorWithHexString:value];
        }] subscribeNext:^(UIColor *x) {
            @strongify(self)
            [self setTitleColor:x forState:UIControlStateNormal];
        }];
    }
    
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    self.layer.cornerRadius = self.bounds.size.height / 2;
    self.layer.borderColor = [UIColor blackColor].CGColor;
    self.layer.borderWidth = 1.0f;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
