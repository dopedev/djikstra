//
//  AHNodeView.h
//  Djikstra
//
//  Created by Anthony Hoang on 5/2/16.
//  Copyright © 2016 Anthony Hoang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AHNodeViewModelProtocol.h"
/**
 *  This is a view that represents a node in a graph tree.
 */
@interface AHNodeView : UIButton
/**
 *  The view model that backs this view.
 */
@property (nonatomic, strong) id<AHNodeViewModelProtocol> viewModel;

@end
