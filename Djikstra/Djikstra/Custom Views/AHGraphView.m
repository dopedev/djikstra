//
//  AHGraphView.m
//  Djikstra
//
//  Created by Anthony Hoang on 5/2/16.
//  Copyright © 2016 Anthony Hoang. All rights reserved.
//

#import "AHGraphView.h"

@interface AHGraphView ()
/**
 *  Keep a reference to all nodes in the view.
 */
@property (nonatomic, strong) NSMutableArray <AHNodeView *> *nodeViews;
/**
 *  A queue for selected nodes.
 *  Edges require two nodes to create an edge. The queue is to cache the first selected node while we wait for the second node.
 *  This will also be used in AHGraphViewStateCompleted, where the user can select two nodes to request the shortest path between the nodes.
 */
@property (nonatomic, strong) NSMutableArray <AHNodeView *> *nodeQueue;
/**
 *  keeping a reference to all of the edges in the view.
 */
@property (nonatomic, strong) NSMutableArray <UIBezierPath *> *edges;


@end

@implementation AHGraphView

/**
 * Lazily insantiate 'nodeViews'. Properties would typically be instantiated in 'inits', but we lazily instantiate this because we don't know which 'init' would be used to create this class.
 */
- (NSMutableArray<AHNodeView *> *)nodeViews {
    
    if (!_nodeViews) {
        _nodeViews = @[].mutableCopy;
        _nodeQueue = @[].mutableCopy;
    }
    
    return _nodeViews;
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    [super touchesBegan:touches withEvent:event];
    
    if (event.type == UIEventTypeTouches && touches.anyObject.tapCount == 1 && self.state == AHGraphViewStateAddNode) {
        AHNodeView *nodeView = [self createAndAddNodeViewToView];
        // add node at point of touch
        CGPoint center = [touches.anyObject locationInView:self];
        nodeView.center = center;
        // alert the delegate that a new node was created.
        [self.delegate graphView:self didCreateNode:nodeView atIndex:[self.nodeViews indexOfObject:nodeView]];
    }
    
    
}

- (void)nodeTapped:(AHNodeView *)node {
    @weakify(self)
    // alert delegate that a node was selected
    [self.delegate graphView:self didSelectNode:node atIndex:[self.nodeViews indexOfObject:node]];
    
    // only allow selection actions when not in AddNode state.
    if (self.state == AHGraphViewStateAddEdge || self.state == AHGraphViewStateComplete) {
        // Toggle the node's selected state.
        node.selected = !node.selected;
        
        if (node.selected) { // if the node is selected, begin creating edge.
            [self.nodeQueue addObject:node];
            
            if (self.nodeQueue.count == 2) {
                
                AHNodeView *startingNode = self.nodeQueue[0];
                AHNodeView *endingNode = self.nodeQueue[1];
                
                if (self.state == AHGraphViewStateAddEdge) {
                    [self createEdge:startingNode endingNode:endingNode]; // create an edge using the nodes in the que
                } else if (self.state == AHGraphViewStateComplete) {
                    
                    [self.dataSource graphView:self
                          pathFromStartingNode:startingNode
                             startingNodeIndex:[self.nodeViews indexOfObject:startingNode]
                                     toEndNode:endingNode
                                  endNodeIndex:[self.nodeViews indexOfObject:endingNode]
                                     pathBlock:^(NSArray<NSNumber *> *nodeIndexes) {
                                         
                                         @strongify(self)
                                         
                                         // highlight the path
                                         NSMutableArray *nodesInpath = @[].mutableCopy;
                                         [nodeIndexes enumerateObjectsUsingBlock:^(NSNumber * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                                             AHNodeView *node = self.nodeViews[obj.integerValue];
                                             node.selected = YES;
                                             [nodesInpath addObject:node];
                                         }];
                                         
                                         [self drawPath:nodesInpath withColor:[UIColor yellowColor]];
                                     }];
                }
            }
        } else { // user deslected a node, so we need to remove it from the queue.
            [self.nodeQueue removeObject:node];
        }
    }
    
}

- (void)createEdge:(AHNodeView *)startingNode endingNode:(AHNodeView *)endingNode {
    @weakify(self)
    
    // Draw the line
    [self drawPath:self.nodeQueue withColor:[UIColor blackColor]];
    // Then notify the datasource, to get the weight of the edge.
    [self.dataSource graphView:self
            willDrawEdgeAtNode:startingNode
                 startingIndex:[self.nodeViews indexOfObject:startingNode]
                    endingNode:endingNode endingNodeIndex:[self.nodeViews indexOfObject:endingNode]
                   weightBlock:^(NSUInteger weight) {
                       @strongify(self)
                       
                       CGPoint firstPoint = startingNode.center;
                       CGPoint secondPoint = endingNode.center;
                       
                       UILabel *label = [UILabel new];
                       label.text = [NSString stringWithFormat:@"%@", @(weight)];
                       [label sizeToFit];
                       label.backgroundColor = [UIColor whiteColor];
                       label.center = CGPointMake((firstPoint.x + secondPoint.x)/2, (firstPoint.y + secondPoint.y)/2);
                       [self addSubview:label];
                   }];
    
    
    startingNode.selected = NO;
    endingNode.selected = NO;
    
    [self.nodeQueue removeAllObjects];
}

- (void)drawPath:(NSArray<AHNodeView *> *)nodes withColor:(UIColor *)color {
    @weakify(self)
    [nodes enumerateObjectsUsingBlock:^(AHNodeView * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        @strongify(self)
        
        if (idx <= nodes.count - 2) { // need two nodes to create an edge.
            AHNodeView *startingNode = obj;
            AHNodeView *endingNode = nodes[idx + 1]; // next object in array
            
            
            CGPoint firstPoint = startingNode.center;
            CGPoint secondPoint = endingNode.center;
            
            CAShapeLayer *shapeLayer = [CAShapeLayer layer];
            shapeLayer.frame = self.bounds;
            shapeLayer.lineWidth = 2.0;
            shapeLayer.strokeColor = color.CGColor;
            
            UIBezierPath *path = [UIBezierPath new];
            path.lineWidth = 2.0f;
            [path moveToPoint:firstPoint];
            [path addLineToPoint:secondPoint];
            
            shapeLayer.path = path.CGPath;
            
            [self.layer addSublayer:shapeLayer];
        }
        
        
    }];
}

- (AHNodeView *)createAndAddNodeViewToView {
    AHNodeView *nodeView = [[NSBundle mainBundle] loadNibNamed:@"AHNodeView" owner:nil options:nil].firstObject;
    [self addSubview:nodeView];
    // keep a reference to all the nodes in the view.
    [self.nodeViews addObject:nodeView];
    [nodeView addTarget:self action:@selector(nodeTapped:) forControlEvents:UIControlEventTouchUpInside];
    
    return nodeView;
}

@end