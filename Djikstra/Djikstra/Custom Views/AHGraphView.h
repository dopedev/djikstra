//
//  AHGraphView.h
//  Djikstra
//
//  Created by Anthony Hoang on 5/2/16.
//  Copyright © 2016 Anthony Hoang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AHGraphViewModelProtocol.h"
#import "AHNodeView.h"

@protocol AHGraphViewDelegate;
@protocol AHGraphViewDataSource;

/**
 *  Defines a new type that represents the state of the graph view.
 */
typedef NS_ENUM(NSInteger, AHGraphViewState) {
    /**
     *  The graph view is in an editing state, the user can still add nodes to the graph.
     */
    AHGraphViewStateAddNode,
    /**
     *  The graph view is in an editing state, the user can add edges to the graph.
     */
    AHGraphViewStateAddEdge,
    /**
     *  The graph view is finished editing. The user can no longer add nodes to the graph.
     */
    AHGraphViewStateComplete
};

/**
 *  This is a custom view that allows the user to draw a graph. Tapping adds a node to the view.
 */
@interface AHGraphView : UIView
/**
 *  Delegate used to receive callbacks from the graph view.
 */
@property (nonatomic, weak) id<AHGraphViewDelegate> delegate;
/**
 *  Datasource used to populate the views.
 */
@property (nonatomic, weak) id<AHGraphViewDataSource> dataSource;
/**
 *  The state of the graph view. Changing this state will change the way the user can interact with the view.
 *  If the state is AHGraphViewStateAddNode, when the user clicks on any empty space in the view, a new node will be added to the graph.
 *  If the state is AHGraphViewStateAddEdge, when the user clicks on any empty space in the view, nothing happens. When the user selects the first node, the node becomes highlighted and the graph will allow the user to select a second node. When the user selects the second node, an edge is created between the two nodes.
 *  If the state is AHGraphViewStateCompleted, interaction is not allowed in the view.
 */
@property (nonatomic, assign) AHGraphViewState state;

@end

/**
 *  Delegate for callbacks from the graph view.
 */
@protocol AHGraphViewDelegate <NSObject>
/**
 *  Alerts the delegate that a new node was created in the graph.
 *
 *  @param graphView The graph the node was created in.
 *  @param node      The node view that was created.
 *  @param index     The index of the node in the view.
 */
- (void)graphView:(AHGraphView *)graphView didCreateNode:(AHNodeView *)node atIndex:(NSUInteger)index;
/**
 *  Alerts the delegate that the user selected a node.
 *
 *  @param graphView The graph view the node is in.
 *  @param node      The node view that was selected.
 *  @param index     The index of the node in the view.
 */
- (void)graphView:(AHGraphView *)graphView didSelectNode:(AHNodeView *)node atIndex:(NSUInteger)index;

@end
/**
 *  Data source used to populate the views of the graph view.
 */
@protocol AHGraphViewDataSource <NSObject>
/**
 *  Alerts the data source that the view finished drawing an edge between two nodes.
 *
 *  @param graphView         The graph view where the edge was drawn.
 *  @param startingNode      The starting node of the edge.
 *  @param startingNodeIndex The starting node's index in the view.
 *  @param endingNode        The ending node of the edge.
 *  @param endingNodeIndex   The ending node's index in the view.
 *  @param weightBlock       The weight block. The weight of the edge that the graph should display should be returned in this block.
 */
- (void)graphView:(AHGraphView *)graphView
willDrawEdgeAtNode:(AHNodeView *)startingNode
    startingIndex:(NSUInteger)startingNodeIndex
       endingNode:(AHNodeView *)endingNode
  endingNodeIndex:(NSUInteger)endingNodeIndex
      weightBlock:(void (^)(NSUInteger weight))weightBlock;
/**
 *  Asks the data source for the path from two nodes. The graph view will highlight the path.
 *
 *  @param graphView         The graph view requesting the path.
 *  @param startingNode      The starting node of the path.
 *  @param startingNodeIndex The index of the starting node in the graph view.
 *  @param endNode           The end node of the path.
 *  @param endNodeIndex      The index of the end node in the graph view.
 *  @param pathBlock         Return an array of NSNumber objects in the block. Each NSNumber should represent the index of the NodeView in the path that should be highlighted. Each index in the array should be in sequential order of the path. The first object should represent the starting index  of the node in the path and the last object should represent the index for the last node in the path.
 */
- (void)graphView:(AHGraphView *)graphView
pathFromStartingNode:(AHNodeView *)startingNode
startingNodeIndex:(NSUInteger)startingNodeIndex
        toEndNode:(AHNodeView *)endNode
     endNodeIndex:(NSUInteger)endNodeIndex
        pathBlock:(void (^)(NSArray <NSNumber *> *nodeIndexes))pathBlock;

@end
