//
//  main.m
//  Djikstra
//
//  Created by Anthony Hoang on 5/2/16.
//  Copyright © 2016 Anthony Hoang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "TestAppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        
        Class appDelegateClass = [AppDelegate class];
        
        // If the app is launched from an XCTestCase file, launch the test app delegate.
        if (NSClassFromString(@"XCTestCase") != nil) {
            appDelegateClass = [TestAppDelegate class];
        }
        return UIApplicationMain(argc, argv, nil, NSStringFromClass(appDelegateClass));
    }
}
