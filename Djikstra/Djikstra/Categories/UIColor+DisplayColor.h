//
//  UIColor+DisplayColor.h
//  Djikstra
//
//  Created by Anthony Hoang on 5/3/16.
//  Copyright © 2016 Anthony Hoang. All rights reserved.
//

#import <UIKit/UIKit.h>
/**
 *  Category methods used to calculate a readable foreground color.
 */
@interface UIColor (DisplayColor)
/**
 *  Calculates a readable foreground color in contrast to the current color.
 *
 *  @return The readable foreground color in hexidecimal.
 */
- (NSString *)calculateDisplayColor;

@end
