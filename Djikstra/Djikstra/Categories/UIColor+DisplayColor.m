//
//  UIColor+DisplayColor.m
//  Djikstra
//
//  Created by Anthony Hoang on 5/3/16.
//  Copyright © 2016 Anthony Hoang. All rights reserved.
//

#import "UIColor+DisplayColor.h"

@implementation UIColor (DisplayColor)

- (NSString *)calculateDisplayColor {
    CGFloat red = 0.0, green = 0.0, blue = 0.0, alpha = 0.0;

    [self getRed:&red green:&green blue:&blue alpha:&alpha];
    
    float threshold = 0.5f;
    float bgDelta = ((red * 0.299) + (green * 0.587) + (blue * 0.114));
    
    NSString *foregroundColor;
    if (bgDelta > threshold) {
        foregroundColor =  @"000000";
    } else {
        foregroundColor = @"ffffff";
    }
    
    return foregroundColor;
}

@end
