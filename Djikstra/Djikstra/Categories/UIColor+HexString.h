//
//  UIColor+HexString.h
//  Djikstra
//
//  Created by Anthony Hoang on 5/3/16.
//  Copyright © 2016 Anthony Hoang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (HexString)
/**
 *  Returns a UIColor object from a hexedecimal string.
 *
 *  @param hexString The hexidecmial representation of the color.
 *
 *  @return The Color.
 */
+ (UIColor *)colorWithHexString:(NSString *)hexString;

@end
