//
//  AHColorsClient.h
//  Djikstra
//
//  Created by Anthony Hoang on 5/3/16.
//  Copyright © 2016 Anthony Hoang. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <ReactiveCocoa/ReactiveCocoa.h>
#import "AHColor.h"

/**
 *  Network reqeusts to a Color service.
 */
@interface AHColorsClient : NSObject
/**
 *  Gets a random color from the service.
 *
 *  @return A signal whose 'next' will return an AHColor object and complete.
 */
- (RACSignal *)getRandomColorSignal;

@end
