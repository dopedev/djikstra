//
//  AHColorsClient.m
//  Djikstra
//
//  Created by Anthony Hoang on 5/3/16.
//  Copyright © 2016 Anthony Hoang. All rights reserved.
//

#import "AHColorsClient.h"
#import <AFNetworking-RACExtensions/AFHTTPSessionManager+RACSupport.h>

static NSString * const kBaseURL = @"http://www.colr.org/json";

@interface AHColorsClient ()

@property (nonatomic, strong) AFHTTPSessionManager *sessionManager;

@end

@implementation AHColorsClient

- (instancetype)init {
    self = [super init];
    
    if (self) {
        _sessionManager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:kBaseURL]];
        _sessionManager.responseSerializer = [AFJSONResponseSerializer serializer];
        _sessionManager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/plain", nil];
        _sessionManager.requestSerializer = [AFJSONRequestSerializer serializer];
        [_sessionManager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"content-type"];
    }
    
    return self;
}

- (RACSignal *)getRandomColorSignal {
    return [[[self.sessionManager rac_GET:@"color/random" parameters:nil] map:^id(RACTuple *tuple) {
        return tuple.first;
    }] map:^AHColor *(id json) {
        return [MTLJSONAdapter modelOfClass:[AHColor class] fromJSONDictionary:json error:nil];
    }];
}

@end
