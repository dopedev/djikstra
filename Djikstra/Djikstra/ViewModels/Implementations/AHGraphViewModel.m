//
//  AHGraphViewModel.m
//  Djikstra
//
//  Created by Anthony Hoang on 5/2/16.
//  Copyright © 2016 Anthony Hoang. All rights reserved.
//

#import "AHGraphViewModel.h"
#import "AHNodeViewModel.h"
#import <PESGraph/PESGraph.h>
#import <PESGraph/PESGraphNode.h>
#import <PESGraph/PESGraphEdge.h>
#import <PESGraph/PESGraphRoute.h>
#import <PESGraph/PESGraphRouteStep.h>
#import "AHColorsClient.h"

@interface AHGraphViewModel ()
/**
 *  All of the created nodes.
 */
@property (nonatomic, strong) NSMutableArray <PESGraphNode *> *nodes;
/**
 *  The graph
 */
@property (nonatomic, strong) PESGraph *graph;
/**
 *  Network client used to get a random color for the nodes.
 */
@property (nonatomic, strong) AHColorsClient *colorsClient;

@end

@implementation AHGraphViewModel

- (instancetype)init {
    self = [super init];
    
    if (self) {
        _nodes = @[].mutableCopy;
        _graph = [PESGraph new];
        _colorsClient = [AHColorsClient new];
    }
    
    return self;
}


#pragma mark - Protocol Methods

- (RACSignal *)createNodeSignal {
    @weakify(self)
    return [RACSignal createSignal:^RACDisposable *(id<RACSubscriber> subscriber) {
        @strongify(self)
        // get random color from service.
        
        PESGraphNode *node = [PESGraphNode new];
        [self.nodes addObject:node];
        node.identifier = [NSString stringWithFormat:@"%@", @(self.nodes.count - 1)];
        node.title = node.identifier;
        
        
        AHNodeViewModel *viewModel = [[AHNodeViewModel alloc] initWithNode:node];
       
        RAC(viewModel, nodeColorHexString) = [[self.colorsClient getRandomColorSignal] map:^NSString *(AHColor* value) {
            return value.hexStringColor;
        }];
        
        [subscriber sendNext:viewModel];
        [subscriber sendCompleted];
        
        return nil;
    }];
}

- (void)createEdgeWithWeight:(NSUInteger)weight
           startingNodeIndex:(NSUInteger)startingNodeIndex
             endingNodeIndex:(NSUInteger)endingNodeIndex {
    
    // make sure the nodes exist.
    if (self.nodes.count >= endingNodeIndex + 1 && self.nodes.count >= startingNodeIndex) {
        
        // create the edge and add it to the graph
        PESGraphNode *startingNode = self.nodes[startingNodeIndex];
        PESGraphNode *endingNode = self.nodes[endingNodeIndex];
        PESGraphEdge *edge = [PESGraphEdge edgeWithName:[NSString stringWithFormat:@" %@ -> %@", startingNode.identifier, endingNode.identifier] andWeight:@(weight)];
        
        [self.graph addEdge:edge fromNode:startingNode toNode:endingNode];
    }
}

-(void)getPathFromStartingNode:(NSUInteger)startingNodeIndex
                     toEndNode:(NSUInteger)endNodeIndex
                  pathCallback:(void (^)(NSArray<NSNumber *> *))pathCallBack {
    
    // make sure the nodes exist.
    if (self.nodes.count >= endNodeIndex + 1 && self.nodes.count >= startingNodeIndex) {
        PESGraphRoute *shorestRoute = [self.graph shortestRouteFromNode:self.nodes[startingNodeIndex] toNode:self.nodes[endNodeIndex]];
        
        NSMutableArray *nodeIndexes = @[].mutableCopy;
        [shorestRoute.steps enumerateObjectsUsingBlock:^(PESGraphRouteStep *obj, NSUInteger idx, BOOL * _Nonnull stop) {
            NSUInteger nodeIndex = [self.nodes indexOfObject:obj.node];
            [nodeIndexes addObject:@(nodeIndex)];
            
        }];
        
        if (pathCallBack) {
            pathCallBack(nodeIndexes.copy);
        }
    }
}

#pragma mark - Private Methods



@end
