//
//  AHNodeViewModel.m
//  Djikstra
//
//  Created by Anthony Hoang on 5/2/16.
//  Copyright © 2016 Anthony Hoang. All rights reserved.
//

#import "AHNodeViewModel.h"
#import "UIColor+HexString.h"
#import "UIColor+DisplayColor.h"
#import <ReactiveCocoa/ReactiveCocoa.h>

@interface AHNodeViewModel ()
/**
 *  The node data that backs this view model.
 */
@property (nonatomic, strong) PESGraphNode *node;

@end

@implementation AHNodeViewModel

- (instancetype)initWithNode:(PESGraphNode *)node {
    self = [super init];
    
    if (self) {
        _node = node;
        
        RAC(self, nodeName) = RACObserve(self, node.title);
        
        RAC(self, textColorHexString) = [[[RACObserve(self, nodeColorHexString) ignore:nil]
                                          map:^UIColor *(NSString *value) {
                                              return [UIColor colorWithHexString:value];
                                          }] map:^NSString *(UIColor *value) {
                                              return [value calculateDisplayColor];
                                          }];
    }
    
    return self;
}

@end
