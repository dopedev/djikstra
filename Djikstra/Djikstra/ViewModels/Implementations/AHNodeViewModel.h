//
//  AHNodeViewModel.h
//  Djikstra
//
//  Created by Anthony Hoang on 5/2/16.
//  Copyright © 2016 Anthony Hoang. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AHNodeViewModelProtocol.h"
#import <PESGraph/PESGraphNode.h>

@interface AHNodeViewModel : NSObject <AHNodeViewModelProtocol>
/**
 *  Designated initalizer.
 *
 *  @param node The node data that backs this view model.
 *
 *  @return The new instance.
 */
- (instancetype)initWithNode:(PESGraphNode *)node;

#pragma mark - Protocol
/**
 *  The view should bind to this property and display the name of the node.
 */
@property (nonatomic, strong) NSString *nodeName;
/**
 *  The view should bind to this property and display the color of the node.
 */
@property (nonatomic, strong) NSString *nodeColorHexString;
/**
 *  The view should bind to this property for the text color of the title label.
 */
@property (nonatomic, strong) NSString *textColorHexString;

@end
