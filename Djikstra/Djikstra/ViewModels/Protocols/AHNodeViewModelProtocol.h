//
//  AHNodeViewModelProtocol.h
//  Djikstra
//
//  Created by Anthony Hoang on 5/2/16.
//  Copyright © 2016 Anthony Hoang. All rights reserved.
//

#import <Foundation/Foundation.h>
/**
 *  This protocol defines the interfaces for a view model that backs a node view.
 */
@protocol AHNodeViewModelProtocol <NSObject>
/**
 *  The view should bind to this property and display the name of the node.
 */
@property (nonatomic, strong) NSString *nodeName;
/**
 *  The view should bind to this property and display the color of the node.
 */
@property (nonatomic, strong) NSString *nodeColorHexString;
/**
 *  The view should bind to this property for the text color of the title label.
 */
@property (nonatomic, strong) NSString *textColorHexString;

@end
