//
//  AHEdgeViewModelProtocol.h
//  Djikstra
//
//  Created by Anthony Hoang on 5/2/16.
//  Copyright © 2016 Anthony Hoang. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AHNodeViewModelProtocol.h"
#import <ReactiveCocoa/ReactiveCocoa.h>
/**
 *  This is a protocol that defines a viewmodel that backs a graph view controller.
 */
@protocol AHGraphViewModelProtocol <NSObject>
/**
 *  The view should subscribe to this signal when the user creates a new node view.
 *
 *  @return A signal whose 'next' returns an 'id<AHNodeViewModelProtocol>' object for the new node view, then completes.
 */
- (RACSignal *)createNodeSignal;
/**
 *  The view should call this method when the user creates an edge between two nodes.
 *
 *  @param weight            The weight of the edge.
 *  @param startingNodeIndex The index of the starting node in the path.
 *  @param endingNodeIndex   The index of the ending node in the path.
 */
- (void)createEdgeWithWeight:(NSUInteger)weight
           startingNodeIndex:(NSUInteger)startingNodeIndex
             endingNodeIndex:(NSUInteger)endingNodeIndex;
/**
 *  The view should call this method to request the shorest path of nodes in the view.
 *
 *  @param startingNodeIndex The index of the starting node in the path.
 *  @param endNodeIndex      The index of the ending node in the path.
 *  @param pathCallBack      The view model should return an NSArray of NSNumbers representing the the index of each node in the path.
 */
- (void)getPathFromStartingNode:(NSUInteger)startingNodeIndex
                      toEndNode:(NSUInteger)endNodeIndex
                   pathCallback:(void (^)(NSArray <NSNumber *> *nodesInPath))pathCallBack;

@end
