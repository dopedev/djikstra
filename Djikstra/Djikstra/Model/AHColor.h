//
//  AHColor.h
//  Djikstra
//
//  Created by Anthony Hoang on 5/3/16.
//  Copyright © 2016 Anthony Hoang. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Mantle/Mantle.h>
/**
 *  Represents a color object
 */
@interface AHColor : MTLModel <MTLJSONSerializing>
/**
 *  The hexidecimal representaion of the color.
 */
@property (nonatomic, strong) NSString *hexStringColor;
@end
