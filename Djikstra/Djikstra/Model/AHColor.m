//
//  AHColor.m
//  Djikstra
//
//  Created by Anthony Hoang on 5/3/16.
//  Copyright © 2016 Anthony Hoang. All rights reserved.
//

#import "AHColor.h"

@implementation AHColor

+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{@"hexStringColor" : @"colors"};
}

+ (NSValueTransformer *)hexStringColorJSONTransformer {
    return [MTLValueTransformer transformerUsingForwardBlock:^id(NSArray *value, BOOL *success, NSError *__autoreleasing *error) {
        return value.firstObject[@"hex"];
    }];
}

@end
