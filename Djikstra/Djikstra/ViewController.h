//
//  ViewController.h
//  Djikstra
//
//  Created by Anthony Hoang on 5/2/16.
//  Copyright © 2016 Anthony Hoang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AHGraphViewModelProtocol.h"

@interface ViewController : UIViewController
/**
 *  The view model used to back this view controller
 */
@property (nonatomic, strong) id<AHGraphViewModelProtocol> viewModel;

@end

