//
//  ViewController.m
//  Djikstra
//
//  Created by Anthony Hoang on 5/2/16.
//  Copyright © 2016 Anthony Hoang. All rights reserved.
//

#import "ViewController.h"
#import "AHGraphView.h"
#import "AHGraphViewModel.h"

@interface ViewController () <AHGraphViewDelegate, AHGraphViewDataSource>

@property (weak, nonatomic) IBOutlet AHGraphView *graphView;
@property (weak, nonatomic) IBOutlet UISegmentedControl *segmentedControl;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.segmentedControl addTarget:self action:@selector(segmentedControlToggled:) forControlEvents:UIControlEventValueChanged];
    
    self.viewModel = [AHGraphViewModel new];
    self.graphView.delegate = self;
    self.graphView.dataSource = self;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)segmentedControlToggled:(UISegmentedControl *)segmentedControl {
    
    switch (segmentedControl.selectedSegmentIndex) {
        case 0:
            self.graphView.state = AHGraphViewStateAddNode;
            break;
        case 1:
            self.graphView.state = AHGraphViewStateAddEdge;
            break;
        case 2:
            self.graphView.state = AHGraphViewStateComplete;
            break;
            
        default:
            break;
    }
}


#pragma mark - AHGraphViewDelegate

- (void)graphView:(AHGraphView *)graphView
    didCreateNode:(AHNodeView *)node
          atIndex:(NSUInteger)index {
    
    @weakify(node)
    
    // Tell view model to create a new node.
    [[self.viewModel createNodeSignal]
     subscribeNext:^(id<AHNodeViewModelProtocol> x) {
         @strongify(node)
         node.viewModel = x;
     }];
    
}

- (void)graphView:(AHGraphView *)graphView
    didSelectNode:(AHNodeView *)node
          atIndex:(NSUInteger)index {
    
}

#pragma mark - AHGraphViewDataSource

- (void)graphView:(AHGraphView *)graphView
willDrawEdgeAtNode:(AHNodeView *)startingNode
    startingIndex:(NSUInteger)startingNodeIndex
       endingNode:(AHNodeView *)endingNode
  endingNodeIndex:(NSUInteger)endingNodeIndex
      weightBlock:(void (^)(NSUInteger))weightBlock {
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Edge Weight" message:@"Select Weight For This Edge" preferredStyle:UIAlertControllerStyleAlert];
    
    [alertController addTextFieldWithConfigurationHandler:^(UITextField * _Nonnull textField) {
       textField.placeholder = @"Enter Weight";
        textField.keyboardType = UIKeyboardTypeNumberPad;
    }];
    
    UIAlertAction *action = [UIAlertAction actionWithTitle:@"Okay" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        UITextField *textField = alertController.textFields.firstObject;
        NSUInteger selectedWeight = [textField.text integerValue];
        
        if (weightBlock) {
            weightBlock(selectedWeight);
        }
        
        // create edge in view model.
        [self.viewModel createEdgeWithWeight:selectedWeight startingNodeIndex:startingNodeIndex endingNodeIndex:endingNodeIndex];

    }];
    
    [alertController addAction:action];
    [self presentViewController:alertController animated:YES completion:nil];
    
}

- (void)graphView:(AHGraphView *)graphView
pathFromStartingNode:(AHNodeView *)startingNode
startingNodeIndex:(NSUInteger)startingNodeIndex
        toEndNode:(AHNodeView *)endNode
     endNodeIndex:(NSUInteger)endNodeIndex
        pathBlock:(void (^)(NSArray<NSNumber *> *))pathBlock {
    [self.viewModel getPathFromStartingNode:startingNodeIndex toEndNode:endNodeIndex pathCallback:pathBlock];
    
}

@end
